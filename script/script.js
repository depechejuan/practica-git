"use strict";

import {
    modalWelcome,
    acierto,
    fallo,
    modalWin,
    modalLose,
    vaciarModal,
    modalInfo,
    h2Title,
    parrafo1,
    parrafo2,
    parrafo3,
    openModal,
    nombreJugador,
} from "./modal.js";

/*
    ################################
    ****    DECLARAMOS EL DOM   ****
    ################################
*/

const rgbElement = document.getElementById("rgb");
let rgbrepresentado = document.getElementById("rgbrepresentado");
let colorElements = document.querySelectorAll("#colores > div");
const divColores = document.querySelectorAll("#colores");
const aciertosElement = document.getElementById("aciertos");
const fallosElement = document.getElementById("fallos");
const resetButton = document.getElementById("reset");
const principianteButton = document.querySelector("#modeP");
const modal = document.querySelector("#openModal");
const mainElement = document.querySelector("main");
const btnPlay = document.getElementById("MusicPlay");
const btnPause = document.getElementById("MusicPause");
const sonidoF = document.getElementById("sonidoF");

/*
    ####################################
    ****    BOTONES PLAY Y PAUSE    ****
    ####################################
*/

btnPlay.addEventListener("mousedown", function () {
    sonidoF.play();
    sonidoF.volume = 0.5;
});

btnPause.addEventListener("mousedown", function () {
    sonidoF.pause();
});

/*
    ###################################
    ****    DECLARAMOS VARIABLES   ****
    ###################################
*/

let aciertos = 0;
let fallos = 0;
let numMin = 0;
let numMax = 255;
let [r, g, b, correctColor] = "";

/*
    ####################################
    ****    ESCRIBIMOS EN EL DOM    ****
    ####################################
*/

aciertosElement.textContent = `Aciertos: ${aciertos}`;
fallosElement.textContent = `Fallos: ${fallos}`;

/*
    ######################################
    ****    CREAMOS UN TEMPORIZADOR   ****
    ######################################
*/

let tiempoTranscurrido = 0;
let temporizador = null;
let tiempoInicio = 60000;
let tiempoRestante = tiempoInicio;
let temporizadorDiv = document.createElement("div");
temporizadorDiv.setAttribute("id", "temporizador");
temporizadorDiv.innerHTML = "1:00";
mainElement.appendChild(temporizadorDiv);

export function startTimer() {
    temporizador = setInterval(function () {
        tiempoRestante -= 1000;
        let minutos = Math.floor(tiempoRestante / 60000);
        let segundos = Math.floor((tiempoRestante % 60000) / 1000);
        let segundosTexto = segundos < 10 ? "0" + segundos : segundos;
        document.getElementById("temporizador").innerHTML =
            minutos + ":" + segundosTexto;
        if (tiempoRestante <= 0) {
            clearInterval(temporizador);
            resetTimer();
            document.getElementById("temporizador").innerHTML =
                "¡Tiempo agotado!";
        }
    }, 1000);
}

export function resetTimer() {
    clearInterval(temporizador);
    tablaPosiciones.push({
        nombre: nombreJugador,
        tiempo: (tiempoInicio - tiempoRestante) / 1000,
    });
    tablaPosiciones.sort(function (a, b) {
        return a.tiempo - b.tiempo;
    });
    localStorage.setItem("tablaPosiciones", JSON.stringify(tablaPosiciones));
    tiempoInicio = 60000;
    tiempoRestante = tiempoInicio;
    temporizadorDiv.innerHTML = "1:00";
    actualizarTabla();
}

export function stopTimer() {
    clearInterval(temporizador);
}

function manejarAcierto() {
    aciertos++;
    if (aciertos >= 3) {
        clearInterval(temporizador);
        resetTimer();
        document.getElementById("temporizador").innerHTML = "¡Has ganado!";
    }
}

function manejarFallo() {
    fallos++;
    if (fallos >= 3) {
        clearInterval(temporizador);
        resetTimer();
        document.getElementById("temporizador").innerHTML = "Has perdido :(";
    }
}

startTimer();

/*
    ################################
    ****    CREAMOS UNA TABLA   ****
    ################################
*/

let tablaPosiciones = [];

// botón para reiniciar tabla de posiciones
let reiniciarBtn = document.createElement("button");
reiniciarBtn.textContent = "Reiniciar tabla de posiciones";
reiniciarBtn.id = "modeA";
reiniciarBtn.addEventListener("click", function () {
    localStorage.removeItem("tablaPosiciones");
    tablaPosiciones = [];
    actualizarTabla();
});
mainElement.appendChild(reiniciarBtn);

// local storage
let tablaPosicionesGuardada = localStorage.getItem("tablaPosiciones");
if (tablaPosicionesGuardada) {
    tablaPosiciones = JSON.parse(tablaPosicionesGuardada);
}

// tabla HTML
let tablaDiv = document.createElement("div");
tablaDiv.innerHTML = "<h2>Tabla de posiciones</h2>";
let tabla = document.createElement("table");
let encabezado = tabla.insertRow();
encabezado.insertCell().textContent = "Posición";
encabezado.insertCell().textContent = "Nombre";
encabezado.insertCell().textContent = "Tiempo";
for (let i = 0; i < tablaPosiciones.length; i++) {
    let fila = tabla.insertRow();
    fila.insertCell().textContent = i + 1;
    fila.insertCell().textContent = tablaPosiciones[i].nombre;
    fila.insertCell().textContent = tablaPosiciones[i].tiempo + " segundos";
}
tablaDiv.appendChild(tabla);
mainElement.appendChild(tablaDiv);

function actualizarTabla() {
    tabla.innerHTML = "";
    encabezado = tabla.insertRow();
    encabezado.insertCell().textContent = "Posición";
    encabezado.insertCell().textContent = "Nombre";
    encabezado.insertCell().textContent = "Tiempo";
    for (let i = 0; i < tablaPosiciones.length; i++) {
        if (tablaPosiciones[i].jugado) {
            let fila = tabla.insertRow();
            fila.insertCell().textContent = i + 1;
            fila.insertCell().textContent = tablaPosiciones[i].nombre;
            fila.insertCell().textContent =
                tablaPosiciones[i].tiempo + " segundos";
        }
    }
    tablaDiv.appendChild(tabla);
}

/*
    ###################################
    ****    FUNCIÓN GENERAR RGB    ****
    ###################################
*/

function generateColors(numMin, numMax) {
    const r = Math.floor(Math.random() * (numMax - numMin + 1) + numMin);
    const g = Math.floor(Math.random() * (numMax - numMin + 1) + numMin);
    const b = Math.floor(Math.random() * (numMax - numMin + 1) + numMin);
    correctColor = [r, g, b];
    return [r, g, b, correctColor];
}

/*
    #####################################
    ****    IMPRIMIMOS LOS COLORES   ****
    #####################################
*/

[r, g, b, correctColor] = generateColors(numMin, numMax);
rgbElement.innerHTML = `${r}, ${g}, ${b}`;
rgbrepresentado.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;

/*
    ###################################
    ****    FUNCIÓN PARA RESETEAR  ****
    ###################################
*/

function resetGame() {
    aciertos = 0;
    fallos = 0;
    aciertosElement.textContent = "Aciertos: 0";
    fallosElement.textContent = "Fallos: 0";
    resetTimer();
    startTimer();
    [r, g, b, correctColor] = generateColors(numMin, numMax);
    variations();
    rgbElement.innerHTML = `${r}, ${g}, ${b}`;
    rgbrepresentado.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
}

/*
    ###################################
    ****    FUNCIÓN PARA GENERAR   ****
            CODIGOS ALEATORIOS     ****
                DEL RGB            ****
    ###################################
*/

function variations() {
    const variation1 = `${Math.min(
        Math.max(0, r + Math.floor(Math.random() * 51) - 25),
        255
    )}, ${Math.min(
        Math.max(0, g + Math.floor(Math.random() * 51) - 25),
        255
    )}, ${Math.min(Math.max(0, b + Math.floor(Math.random() * 51) - 25), 255)}`;
    const variation2 = `${Math.min(
        Math.max(0, r + Math.floor(Math.random() * 51) - 25),
        255
    )}, ${Math.min(
        Math.max(0, g + Math.floor(Math.random() * 51) - 25),
        255
    )}, ${Math.min(Math.max(0, b + Math.floor(Math.random() * 51) - 25), 255)}`;
    const variation3 = `${Math.min(
        Math.max(0, r + Math.floor(Math.random() * 51) - 25),
        255
    )}, ${Math.min(
        Math.max(0, g + Math.floor(Math.random() * 51) - 25),
        255
    )}, ${Math.min(Math.max(0, b + Math.floor(Math.random() * 51) - 25), 255)}`;
    const variation4 = correctColor;

    // Mezclar las variaciones en un arreglo aleatorio
    const variaciones = [variation1, variation2, variation3, variation4].sort(
        () => Math.random() - 0.5
    );

    // Asignar cada variación a un div correspondiente
    const color1 = document.getElementById("color1");
    color1.style.backgroundColor = `rgb(${variaciones[0]})`;

    const color2 = document.getElementById("color2");
    color2.style.backgroundColor = `rgb(${variaciones[1]})`;

    const color3 = document.getElementById("color3");
    color3.style.backgroundColor = `rgb(${variaciones[2]})`;

    const color4 = document.getElementById("color4");
    color4.style.backgroundColor = `rgb(${variaciones[3]})`;
    return variaciones;
}

variations();

/*
    ###################################
    ****    FUNCIÓN PARA VALIDAR   ****
            EL COLOR ELEGIDO       ****
    ###################################
*/
function validateSelection() {
    colorElements.forEach((element, i) => {
        element.onclick = () => {
            if (element.style.backgroundColor === `rgb(${r}, ${g}, ${b})`) {
                aciertos++;
                aciertosElement.textContent = `Aciertos: ${aciertos}`;
                acierto();
                [r, g, b, correctColor] = generateColors(numMin, numMax);
                variations();
                rgbElement.innerHTML = `${r}, ${g}, ${b}`;
                rgbrepresentado.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
            } else {
                fallos++;
                fallosElement.textContent = `Fallos: ${fallos}`;
                fallo();
                [r, g, b, correctColor] = generateColors(numMin, numMax);
                variations();
                rgbElement.innerHTML = `${r}, ${g}, ${b}`;
                rgbrepresentado.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
            }
            if (aciertos === 3) {
                vaciarModal();
                modalWin();
                openModal();
                resetGame();
            } else if (fallos === 3) {
                vaciarModal();
                modalLose();
                openModal();
                resetGame();
            }
        };
    });
}

validateSelection();

/*
    ###################################
    ****    FUNCIÓN PARA EMPEZAR  ****
    ****     DE NUEVO EL JUEGO    ****
    ###################################
*/

resetButton.addEventListener("click", () => {
    modalWelcome();
    openModal();
    resetGame();
});
