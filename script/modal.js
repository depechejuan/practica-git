"use strict";

import { startTimer, resetTimer, stopTimer } from "./script.js";

const modalInfoSection = document.getElementById("modalInfo");
const openModalButton = document.getElementById("openModal");
const closeModalBtn = document.getElementById("closeModal");
export const h2Title = document.querySelector("#tituloModal");
export const parrafo1 = document.querySelector("#parrafo1");
export const parrafo2 = document.querySelector("#parrafo2");
export const parrafo3 = document.querySelector("#parrafo3");
const parrafo4 = document.querySelector("#parrafo4");
const contenedor = document.getElementById("modalContent");
const formulario = document.createElement("form");
const input = document.createElement("input");
const botonSubmit = document.createElement("button");
export let nombreJugador = input.value;

export function openModal() {
    modalInfoSection.classList.add("modalDialog--open");
}

function closeModal() {
    modalInfoSection.classList.remove("modalDialog--open");
}

// Abre el modal al cargar la página
window.onload = function () {
    modalWelcome();
    openModal();
};

openModalButton.addEventListener("click", function () {
    vaciarModal();
    modalInfo();
    openModal();
});

closeModalBtn.addEventListener("click", function () {
    closeModal();
});

/* ## Modal de Bienvenida ## */
export function modalWelcome() {
    modalInfo();
    closeModalBtn.classList.add("hidden");
    parrafo4.textContent = "Introduce tu nombre de usuario:";
    formulario.appendChild(input);
    input.required = true;
    botonSubmit.textContent = "Enviar";
    formulario.appendChild(botonSubmit);
    botonSubmit.id = "#modeA";
    contenedor.appendChild(formulario);
}

/* ## regulamos el Acierto ## */
export function acierto() {
    const win = new Audio("../audio/win.mp3");
    win.play();
}

/* ## regulamos el Fallo ## */
export function fallo() {
    const lose = new Audio("../audio/lose.mp3");
    lose.play();
}

/* ## Modal de WIN ## */
export function modalWin() {
    const ganaste = new Audio("../audio/ganaste.mp3");
    ganaste.play();
    vaciarModal();
    closeModalBtn.classList.add("hidden");
    h2Title.textContent = "¡Has Ganado! 🎉";
    parrafo1.textContent = `¡Felicidades ${nombreJugador}! ¡Eres todo un Crack!`;
    parrafo2.textContent = "¿Quieres volver a jugar?";
    parrafo3.innerHTML = `
    <button id="siBtn">¡CLARO QUE SÍ!</button>
    <button id="noBtn">¡NO QUE SOY MUY MALO!</button>
    `;
    const siBtn = document.getElementById("siBtn");
    const noBtn = document.getElementById("noBtn");

    siBtn.addEventListener("click", () => {
        closeModal();
        resetTimer();
        startTimer();
    });

    noBtn.addEventListener("click", () => {
        closeModal();
        stopTimer();
    });
}

/* ## Modal de LOSE ## */
export function modalLose() {
    const derrota = new Audio("../audio/derrota.mp3");
    derrota.play();
    vaciarModal();
    closeModalBtn.classList.add("hidden");
    h2Title.textContent = "¡Oh No! ¡Perdiste! 😥 ";
    parrafo1.textContent = `Una lástima ${nombreJugador}!`;
    parrafo2.textContent = "Seguro que fue mala suerte!";
    parrafo3.textContent = "¡Sigue intentándolo!";
    parrafo2.textContent = "¿Quieres volver a jugar?";
    parrafo3.innerHTML = `
    <button id="siBtn">¡CLARO QUE SÍ!</button>
    <button id="noBtn">¡NO QUE SOY MUY MALO!</button>
    `;
    const siBtn = document.getElementById("siBtn");
    const noBtn = document.getElementById("noBtn");

    siBtn.addEventListener("click", () => {
        closeModal();
        resetTimer();
        startTimer();
    });

    noBtn.addEventListener("click", () => {
        stopTimer();
        closeModal();
    });
}

/* ## Vaciamos el Modal */
export function vaciarModal() {
    closeModalBtn.classList.remove("hidden");
    h2Title.textContent = "";
    parrafo1.textContent = "";
    parrafo2.textContent = "";
    parrafo3.textContent = "";
    parrafo4.textContent = "";
    formulario.remove();
    input.remove();
    botonSubmit.remove();
}

export function modalInfo() {
    vaciarModal();
    h2Title.textContent = "INFORMACIÓN";
    parrafo1.textContent = "Tienes que averiguar qué color es el RGB generado.";
    parrafo2.textContent =
        "¡Sólo tienes un intento por color! Así que piensalo bien...";
    parrafo3.innerHTML = `RECUERDA: R(rojo), G(verde), B(azul). <br>¡Usa la lógica de colores y conviértete en un experto!`;
}

botonSubmit.addEventListener("click", function (event) {
    event.preventDefault();
    nombreJugador = input.value;
    console.log(nombreJugador);
    if (nombreJugador.trim() !== "") {
        closeModal();
    }
});
