# Adivina El Color

## Descripción del proyecto:

Este es el primero de nuestros proyectos, y no el último. Lo hemos creado con Gitlab y estamos empezando a mostrar nuestras habilidades y conocimientos.

con:

**_HTML_**, lenguaje para definir el significado y la estructura del contenido web, lenguaje de marcas de hipertexto.

**_CSS_**, hojas de Estilo en Cascada. Lenguaje de estilo que maneja el diseño y presetación de las páginas web, o lo que es lo mismo, cómo las vemos.

**_JavaScript_**, es un lenguaje de programación que que se debe usar si queremos añadir características interactivas a nuestro sitio web(juegos, eventos que ocurren al presionar un boton, introducir datos en un formulario, animaciones, y más).

**_Markdown_**, es un lenguaje de marcado sencillo, que agrega formato (también imágenes, vínculos) a un texto simple le añadimos caracteres extra para enriquecerlo, para que visualmente sea más agradable y sea más fácil su lectura.

> _"Lo importante no es lo que sabes, sino lo que haces con lo que sabes"._

## Objetivos

-   ### Objetivo de la aplicación

         La aplicación debe mostrar la pantalla dividida en dos partes, en ellas podemos ver:

    1.  El código de color RGB(p.ej.255,87,51), que hay que encontrar entre varias cajas de color.
    2.  Se muestra un contador de aciertos y fallos.
    3.  La persona jugadora tiene que hacer click en la caja de color correcto.
    4.  Si se hace click en la caja correcta se acumulará un punto en el contador de aciertos, y si se falla, se acumulará en el de fallos.
    5.  Tras el acierto o fallo el juego debe mostrar un nuevo código de color y nuevas cajas.
    6.  Se gana la partida cuando se llega a 3 aciertos, y se pierde si se llega a 3 fallos.
        Éstos eran los requisitos pedidos.<br

*   ### Objetivos adicionales, creados por el equipo

    1.  Implementación de varios "Modal", para:
        -   Informar del funcionamiento del juego y solicitar su nombre al jugador/-a.
        -   Enviar mensajes de la evolución del juego sin utilizar "alert".
    2.  Creación de un Timer con un máximo de 60 segundos para completar el juego.
    3.  Creación de un "Local Storage" para almacenar ciertas puntuaciones, explicadas a continuación:
        Añadida una tabla (tras la solicitud de un nombre de Jugador mediante un prompt)
    4.  Cuenta el tiempo que has tardado en jugar la partida, y la ordena según el tiempo en el que completas el juego.
    5.  Botones de Play/Pause con música para acompañar el juego.
    6.  Animaciones de CSS
    7.  Implantación de "FavIcon"

## Recursos:

Web de MDN:

-   https://developer.mozilla.org/en-US/docs/Web/CSS/color_value/rgb
-   https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Math/round()

## Explicaciones

-   ### Explicación Básica del código.

    -   Creación del árbol de trabajo en el cual sólo deja el "index.html" en la carpeta raíz. Debajo de ella se crean las sub-carpetas de recursos para una mejor visibilidad y agilidad en el flujo de trabajo.
    -   Se crean 3 archivos JavaScript:
        -   _Modal_ - Donde se incluyen las referencias del Modal y su reutilidad.
        -   _Script_ - Donde ubicamos todo el código JavaScript del juego. La generación del RGB y sus variaciones aleatorias, el DOM para escribir datos en el HTML, y la creación del Timer.
        -   _Utility_ - para la implantación del Local Storage, guardando así las puntuaciones de los jugadores.
    -   Carpetas "Audio/svg/img" con sus recursos.
        El diseño es multiplataforma, por su diseño Responsive

## Detalles de nuestro código

```js
function manejarAcierto() {
    aciertos++;
    console.log("Acertaste! Aciertos: " + aciertos);
    if (aciertos >= 3) {
        clearInterval(temporizador);
        resetTimer();
        document.getElementById("temporizador").innerHTML = "¡Has ganado!";
    }
}
```

```css
@keyframes color-change {
    0% {
        background-color: #ff0080;
    }
    17% {
        background-color: #ff8c00;
    }
    34% {
        background-color: #ffef00;
    }
    51% {
        background-color: #00ff00;
    }
    68% {
        background-color: #00ffff;
    }
    85% {
        background-color: #0000ff;
    }
    100% {
        background-color: #8b00ff;
    }
}
```

## Implementación de Imágenes

![Esta imagen es el Fondo.jpg de nuestro proyecto, y estarás leyendo ésto si la imagen no se puede cargar](img/Fondo.jpg)

> Fondo.jpg

## Instalación y uso

    Actualmente no está disponible la instalación.

Para acceder: **https://randomgamebet.netlify.app/**

## Autores:

    Juan León Medina, Gonzalo Rodriguez Aquino,
    Luis de Marçay y Ana Isabel Navarro Gómez.
